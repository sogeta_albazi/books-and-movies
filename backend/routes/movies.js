const express = require('express')
const db = require('../db')
const router = express.Router()

router.get("/", function(request, response){
	
    db.getAllMovies(function (error, movies) {
		if(error){
			response.status(500).end()
		}
		else{
			response.status(200).json(movies)
		}
	})
})

router.get("/:id", function(request, response){
	
	const id = request.params.id
	db.getMovieById(id, function (error, movie) {
		if(error){
			response.status(500).end()
		}else if(movie){
			response.status(200).json(movie)
		}else{
			response.status(404).end()
		} 
		
	})
   
})

module.exports = router