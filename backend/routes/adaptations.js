const express = require('express')
const db = require('../db')
const router = express.Router()

router.get("/", function(request, response){
	
    db.getAllBookAdaptions(function (error, adaptations) {
		if(error){
			response.status(500).end()
		}
		else{
			response.status(200).json(adaptations)
		}
	})
})

router.get("/:id", function(request, response){
	
	const id = request.params.id
	db.getBookAdaptionById(id, function (error, adaptation) {
		if(error){
			response.status(500).end()
		}else if(adaptation){
			response.status(200).json(adaptation)
		}else{
			response.status(404).end()
		} 
		
	})
   
})

module.exports = router