const express = require('express')
const db = require('../db')
const router = express.Router()

router.get("/", function(request, response){
	
    db.getAllBooks(function (error, books) {
		if(error){
			response.status(500).end()
		}
		else{
			response.status(200).json(books)
		}
		

	})
})
router.get("/:id", function(request, response){
	
	const id = request.params.id
	db.getBookById(id, function (error, book) {
		if(error){
			response.status(500).end()
		}else if(book){
			response.status(200).json(book)
		}else{
			response.status(404).end()
		} 
		
	})
   
})
module.exports = router