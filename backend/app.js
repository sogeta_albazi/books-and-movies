const express = require('express')
const db = require('./db')
const app = express()
const booksRouter = require('./routes/books')
const moviesRouter = require('./routes/movies')
const AdaptationsRouter = require('./routes/adaptations')
//import * as path from "path";
const path = __dirname + '/views/';

app.use(express.static(path));



app.use(function(request, response, next){
	
	response.setHeader("Access-Control-Allow-Origin", "*")
	response.setHeader("Access-Control-Allow-Methods", "*")
	response.setHeader("Access-Control-Allow-Headers", "*")
	response.setHeader("Access-Control-Expose-Headers", "*")
	
	next()
	
})

app.use("/books", booksRouter)
app.use("/movies", moviesRouter)
app.use("/adaptations", AdaptationsRouter)
app.get('/', function (req,res) {
	res.sendFile(path + "index.html");
  });


app.listen(3000)