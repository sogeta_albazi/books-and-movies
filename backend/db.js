const sqlite3 = require('sqlite3')

const db = new sqlite3.Database('./dataBase.db')

db.run("PRAGMA foreign_keys = ON")

exports.getAllData = function (callback) {
	const query = "SELECT * FROM books UNION SELECT * FROM movies"
	db.all(query, function (error, books) {
		callback(error, books)
	})
}
/************************ books *********************/
exports.getAllBooks = function (callback) {
	const query = "SELECT * FROM books"
	db.all(query, function (error, books) {
		callback(error, books)
	})
}

exports.getBookById = function (id, callback) {
	const query = "SELECT * FROM books WHERE bookID = ?"
	const values = [id]
	db.get(query, values, function (error, book) {
		callback(error, book)
	})
}
/************************ movies *********************/
exports.getAllMovies = function (callback) {
	const query = "SELECT * FROM movies"
	db.all(query, function (error, movies) {
		callback(error, movies)
	})
}
exports.getMovieById = function (ID, callback) {
	const query = "SELECT * FROM movies WHERE movieID = ?"
	const values = [ID]
	db.get(query, values, function (error, movie) {
		callback(error, movie)
	})
}
/************************ books adaptations *********************/

exports.getAllBookAdaptions = function (callback) {
	const query = " SELECT * FROM bookAdaptions,books WHERE bookAdaptions.bookID=books.bookID"
	db.all(query, function (error, adaptions) {
		callback(error, adaptions)
	})
}

exports.getBookAdaptionById = function (ID, callback) {
	const query = `SELECT * FROM bookAdaptions,books 
					WHERE bookAdaptions.bookID=books.bookID AND bookAdaptions.bookAdaptionsID=?`
	const values = [ID]
	db.get(query, values, function (error, adaption) {
		callback(error, adaption)
	})
}

