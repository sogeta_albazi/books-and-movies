import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Books from '../views/Books.vue'
import Book from '../views/Book.vue'
import Movies from '../views/Movies.vue'
import Movie from '../views/Movie.vue'
import Adaptations from '../views/Adaptations.vue'
import Adaptation from '../views/Adaptation.vue'
import BookAPI from '../views/BookAPI.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Books
  },
  {
    path: '/about',
    name: 'About',
   
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/About.vue')
    }
  }, 
  {
    path: '/books',
    name: 'Books',
    component: Books
  },
  {
    path: '/books/:id',
    name: 'Book',
    component: Book
  },
  {
    path: '/movies',
    name: 'Movies',
    component: Movies
  },
  {
    path: '/movies/:id',
    name: 'Movie',
    component: Movie
  },
  {
    path: '/adaptations',
    name: 'Adaptations',
    component: Adaptations
  },
  {
    path: '/adaptations/:id',
    name: 'Adaptation',
    component: Adaptation
  },
  {
    path: '/search/:keyword',
    name: 'BookAPI',
    component: BookAPI 
  },


]

const router = new VueRouter({
  routes
})

export default router
